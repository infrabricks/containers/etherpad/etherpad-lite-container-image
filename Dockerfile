# Etherpad Lite Dockerfile
#
# https://github.com/ether/etherpad-lite
# Original author: muxator
# Adapted by PCLL Team for Eole³ project

##
# Build app
##

FROM node:lts-alpine AS adminBuild

ARG ETHERPAD_VERSION=2.1.0
ARG GITREPO_URL="https://gitlab.mim-libre.fr/infrabricks/containers/etherpad/etherpad-lite-sources.git"
ENV NPM_VERSION=10.8.1
ENV PNPM_VERSION=9.1.4

# Set these arguments when building the image from behind a proxy
ENV http_proxy=
ENV https_proxy=
ENV no_proxy=

# Install GIT and clone git repo

RUN apk add --no-cache --update git

RUN cd /opt && git clone --depth 1 --branch v${ETHERPAD_VERSION} \
    -c advice.detachedHead=false ${GITREPO_URL} && cd $(basename -s .git "${GITREPO_URL}") && \
    mkdir -p /app/{admin,ui} && \
    cp ./settings.json.docker /app/settings.json && \
    cp ./pnpm-workspace.yaml /app/ && cp ./package.json /app/ && \
    cp -r ./bin /app/ && cp -r ./src /app/ && cp -r ./var /app/ && \
    cp -r ./admin /app/ && cp -r ./ui /app/

WORKDIR /app

# Build app

RUN cd ./admin && npm install -g npm@${NPM_VERSION} && \
    npm install -g pnpm@${PNPM_VERSION} && pnpm install && pnpm run build --outDir /app/admin/dist
RUN cd ./ui && pnpm install && pnpm run build --outDir /app/ui/dist

##
# addons plugins and conf stage
##

FROM node:lts-alpine AS build

LABEL maintainer="PCLL Team"
LABEL org.opencontainers.image.title="etherpad-lite" \
      org.opencontainers.image.description="Collaborative document editing" \
      org.opencontainers.image.vendor="etherpad.org" \
      org.opencontainers.image.licenses="Apache-2.0" \
      org.opencontainers.image.source="https://github.com/ether/etherpad-lite/" \
      org.opencontainers.image.maintainer="PCLL Team"

# Upgrade npm
RUN npm install -g npm@${NPM_VERSION}

ARG TIMEZONE=

RUN \
  [ -z "${TIMEZONE}" ] || { \
    apk add --no-cache tzdata && \
    cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && \
    echo "${TIMEZONE}" > /etc/timezone; \
  }
ENV TIMEZONE=${TIMEZONE}

# plugins to install while building the container. By default no plugins are
# installed.
# If given a value, it has to be a space-separated, quoted list of plugin names.
#
# EXAMPLE:
#   ETHERPAD_PLUGINS="ep_codepad ep_author_neat"
ARG ETHERPAD_PLUGINS=

# local plugins to install while building the container. By default no plugins are
# installed.
# If given a value, it has to be a space-separated, quoted list of plugin names.
#
# EXAMPLE:
#   ETHERPAD_LOCAL_PLUGINS="../ep_my_plugin ../ep_another_plugin"
ARG ETHERPAD_LOCAL_PLUGINS=

# Control whether abiword will be installed, enabling exports to DOC/PDF/ODT formats.
# By default, it is not installed.
# If given any value, abiword will be installed.
#
# EXAMPLE:
#   INSTALL_ABIWORD=true
ARG INSTALL_ABIWORD=

# Control whether libreoffice will be installed, enabling exports to DOC/PDF/ODT formats.
# By default, it is not installed.
# If given any value, libreoffice will be installed.
#
# EXAMPLE:
#   INSTALL_LIBREOFFICE=true
ARG INSTALL_SOFFICE=

# Install dependencies required for modifying access.
RUN apk add --no-cache shadow bash
# Follow the principle of least privilege: run as unprivileged user.
#
# Running as non-root enables running this image in platforms like OpenShift
# that do not allow images running as root.
#
# If any of the following args are set to the empty string, default
# values will be chosen.
ARG EP_HOME=
ARG EP_UID=5001
ARG EP_GID=0
ARG EP_SHELL=

RUN groupadd --system ${EP_GID:+--gid "${EP_GID}" --non-unique} etherpad && \
    useradd --system ${EP_UID:+--uid "${EP_UID}" --non-unique} --gid etherpad \
        ${EP_HOME:+--home-dir "${EP_HOME}"} --create-home \
        ${EP_SHELL:+--shell "${EP_SHELL}"} etherpad

ARG EP_DIR=/opt/etherpad-lite
RUN mkdir -p "${EP_DIR}" && chown etherpad:etherpad "${EP_DIR}"

# the mkdir is needed for configuration of openjdk-11-jre-headless, see
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=863199
RUN  \
    mkdir -p /usr/share/man/man1 && \
    npm install pnpm@${PNPM_VERSION} -g  && \
    apk update && apk upgrade && \
    apk add --no-cache \
        ca-certificates \
        curl \
        git \
        ${INSTALL_ABIWORD:+abiword abiword-plugin-command} \
        ${INSTALL_SOFFICE:+libreoffice openjdk8-jre libreoffice-common}

USER etherpad

WORKDIR "${EP_DIR}"

COPY --chown=etherpad:etherpad --from=adminBuild /app/settings.json settings.json
COPY --chown=etherpad:etherpad --from=adminBuild /app/var ./var
COPY --chown=etherpad:etherpad --from=adminBuild /app/bin ./bin
COPY --chown=etherpad:etherpad --from=adminBuild /app/pnpm-workspace.yaml /app/package.json ./

##
# Final stage
##

FROM build as production

ENV NODE_ENV=production
ENV ETHERPAD_PRODUCTION=true

COPY --chown=etherpad:etherpad --from=adminBuild /app/src ./src
COPY --chown=etherpad:etherpad --from=adminBuild /app/admin/dist ./src/templates/admin
COPY --chown=etherpad:etherpad --from=adminBuild /app/ui/dist ./src/static/oidc

RUN bin/installDeps.sh && rm -rf ~/.npm && rm -rf ~/.local && rm -rf ~/.cache && \
    if [ ! -z "${ETHERPAD_PLUGINS}" ] || [ ! -z "${ETHERPAD_LOCAL_PLUGINS}" ]; then \
        pnpm run install-plugins ${ETHERPAD_PLUGINS} ${ETHERPAD_LOCAL_PLUGINS:+--path ${ETHERPAD_LOCAL_PLUGINS}}; \
    fi

# Copy the configuration file.
COPY --chown=etherpad:etherpad --from=adminBuild /app/settings.json "${EP_DIR}"/settings.json

# Fix group permissions
# Note: For some reason increases image size from 257 to 334.
# RUN chmod -R g=u .

USER etherpad

HEALTHCHECK --interval=5s --timeout=3s \
  CMD curl --silent http://localhost:9001/health | grep -E "pass|ok|up" > /dev/null || exit 1

EXPOSE 9001
CMD ["pnpm", "run", "prod"]

