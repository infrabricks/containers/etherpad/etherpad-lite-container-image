# Etherpad Lite container builder

This dockerfile builds a container for Etherpad Lite.

## Presentation

Etherpad Lite is an almost complete rewrite of the original Etherpad software, based on different technical foundations and written by different authors. While the original Etherpad is written in Java and Scala and has quite demanding system requirements, Etherpad Lite is written in server-side JavaScript using Node.js. The original realtime synchronization library (called Easysync) remains the same.

Etherpad Lite has some distinctive features which are not available in the original version:

- An HTTP API which allows the user to interact with the pad contents, and with user and group management
- A jQuery plugin exists which helps embedding the collaborative editor in other sites
- Clients for PHP, Python, Ruby, JavaScript, Java, Objective-C and Perl, which interface with the API.
- More than 50 plugins, among them email_notifications, invite_via_email, offline_edit, fileupload, tables or rtc for video calls based on WebRTC.

Etherpad Lite offers a number of export formats, including LaTeX, but as of June 2019, not Markdown. But there is an official addon to export in markdown. Etherpad Lite supports many natural languages. Localization is achieved collaboratively through translatewiki.net. 

[Source](https://en.wikipedia.org/wiki/Etherpad#Etherpad_Lite)

## Usage

Build command example

```
docker build -t local/etherpad-lite:2.0.3 --build-arg="ETHERPAD_VERSION=2.0.3" --progress=plain .
```

## Links

- [Documentation](https://docs.etherpad.org/)
- [Github repo](https://github.com/ether/etherpad-lite)
